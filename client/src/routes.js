export default {
  DASHBOARD: "/dashboard",

  TEAMS_LISTING: "/teams",
  TEAMS_NEW: "/teams/new",
  TEAMS_DETAIL: "/teams/detail/:id",
  TEAMS_EDIT: "/teams/edit/:id",

  MAP_LIVE: "/map-live",
  MAP: "/map",

  getUrl(path, params = {}) {
    return path.replace(/:(\w+)/g, (m, k) => params[k]);
  }
};
