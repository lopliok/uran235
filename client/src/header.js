import { BrowserRouter, NavLink, Route, Switch } from "react-router-dom";
import React, { Component } from "react";

import authService from "./auth-service";
import routes from "./routes";

class Header extends Component {
  render() {
    return (
      <div>
        <nav className="main-header navbar navbar-expand bg-white navbar-light border-bottom justify-content-between">
          <ul className="navbar-nav">
            <li className="nav-item">
              <a className="nav-link" data-widget="pushmenu" href="#">
                <i className="ion-navicon" />
              </a>
            </li>
            <li className="nav-item d-none d-sm-inline-block">
              <NavLink to={routes.MAP_LIVE} className="nav-link">
                Map
              </NavLink>
            </li>
            <li className="nav-item d-none d-sm-inline-block">
              <NavLink to="/teams" className="nav-link">
                Teams
              </NavLink>
            </li>
            <li className="nav-item d-none d-sm-inline-block">
              <NavLink to="/tasks" className="nav-link">
                Tasks
              </NavLink>
            </li>
            <li className="nav-item d-none d-sm-inline-block">
              <NavLink to="/map" className="nav-link">
                Users
              </NavLink>
            </li>
            <li className="nav-item d-none d-sm-inline-block">
              <NavLink to="/map" className="nav-link">
                Location points
              </NavLink>
            </li>
          </ul>

          <form className="form-inline ml-3">
            <div className="input-group input-group-sm">
              <input
                className="form-control form-control-navbar"
                type="search"
                placeholder="Search"
                aria-label="Search"
              />
              <div className="input-group-append">
                <button className="btn btn-navbar" type="submit">
                  <i className="fa fa-search" />
                </button>
              </div>
              <div className="ml-2">
                <button
                  className="btn btn-sm float-right"
                  onClick={authService.logout}
                >
                  logout
                </button>
              </div>
            </div>
          </form>
        </nav>
      </div>
    );
  }
}

export default Header;
