export default {
  converterCoords(lat, long) {
    long = long / Math.pow(10, 6);
    lat = lat / Math.pow(10, 6);
    return [lat, long];
  },

  getSubArraysLocation(array, start, count) {
    let newArray = [];
    if (!array) return [];
    array.forEach(item => {
      newArray.push(item.slice(start, count));
    });
    return newArray;
  }
};
