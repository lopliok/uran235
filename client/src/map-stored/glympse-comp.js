import axios from 'axios'

import glympseService from './glympse-service'

export default {
    Glympse: {
        api_key: '0156a3in2FrS1kxwurOD', 
        config: {
            "headers": {
                "Content-Type": "application/json",
                "Authorization": "Bearer ",
            }
        }
    },

// TODO: separate async function

    async setAccessToken() {
        let res = await glympseService.getCredentials(this.Glympse.api_key)
        let { id, password } = res.data.response;
        this.Glympse = {
            ...this.Glympse, id: id, password: password
        }
        res = await glympseService.getAccessToken(this.Glympse.id, this.Glympse.password, this.Glympse.api_key)
        let { access_token, account_created, expires_in } = res.data.response;
        this.Glympse = {
            ...this.Glympse, access_token: access_token, account_created: account_created, expires_in: expires_in
        }
        this.Glympse.config.headers.Authorization = "Bearer " + this.Glympse.access_token
    },
    
    async initialLocation(codeTeam) {

        !this.Glympse.access_token ? await this.setAccessToken() : null

        const res = await glympseService.initialLocation(codeTeam, this.Glympse.config)
        return res.data.response
    }


}