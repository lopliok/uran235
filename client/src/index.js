import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './include'

import App from './App';

import authService from './auth-service'

const render = () =>
    ReactDOM.render(<App />, document.getElementById('root'));

authService.init()
    
render()

authService.onChange = render

