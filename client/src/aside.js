import React, { Component } from 'react'
import { BrowserRouter, Switch, Route, NavLink, Link } from 'react-router-dom'

import ROUTES from './routes'

import authService from './auth-service'

class Aside extends Component {




    logout() {
        authService.logout();
    }


    render() {


        return (
            <aside className="main-sidebar sidebar-dark-primary elevation-4">

                <div className="sidebar">
                    <div className="user-panel mt-3 pb-3 mb-3 d-flex">

                        <a href="index3.html" style={{paddingTop: '15px'}}>
                            <img src="/re.png" alt="AdminLTE Logo" className="brand-image img-circle elevation-3"  />
                            <span className="brand-text font-weight-light">{authService.user}</span>
                        </a>
                        

                    </div>

                    <nav className="mt-2">
                        <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                            <li className="nav-item">
                                <NavLink to={ROUTES.MAP} className='nav-link' >
                                    <i className="nav-icon fa fa-tree"></i>
                                    <p>
                                        Map
                                    </p>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to={ROUTES.TEAMS_LISTING} exact className='nav-link' >
                                    <i className="nav-icon fa fa-table"></i>
                                    <p>
                                        Teams
                                <span className="right badge badge-danger">1</span>
                                    </p>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to={ROUTES.TEAMS_NEW} className='nav-link' onClick={(e) => console.log(e)} >
                                    <i className="nav-icon fa fa-plus"></i>
                                    <p>
                                        New team
                                    </p>
                                </NavLink>
                            </li>
                            {window.innerWidth < 600 &&
                                <li className="nav-item">
                                    <a className="nav-link" data-widget="pushmenu" href="#">Zavřít <i className="ion-close"></i></a>
                                </li>
                            }
                        </ul>
                    </nav>
                </div>
            </aside>

        )
    }


}


export default Aside