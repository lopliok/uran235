import React, { Component } from 'react'
import { Map as LeafletMap, TileLayer, Marker, Popup } from 'react-leaflet'
import LocationService from './location-sevice'

class Location extends Component {

    state = {
        latlng: {
            lat: null,
            lng: null,
        },
        api_key: '0156a3in2FrS1kxwurOD',
        user_id: null,
        user_password: null
    }

    async createAccount() {
        const res = await LocationService.createAccount(this.state.api_key)
        this.setState({
            user_id: res.id, user_password: res.password
        })
    }
    async login() {
        const res = await LocationService.login(this.state.user_id, this.state.user_password, this.state.api_key)
        this.setState({
            access_token: res.access_token
        })
    }

    async getInitial() {
        const res = await LocationService.initialTicket(this.state.access_token)
    }

    async test() {
        const res = await LocationService.newToken()
    }



    render() {


        return (
            <div style={{ textAlign: 'left' }}>
                <label>
                    fdsfd
        </label>
        <button onClick={() => { this.test() }}>test Glympse API</button>

                <button onClick={() => { this.createAccount() }}>Účet vytvořit</button>
                {this.state.user_id != null && <button onClick={() => { this.login() }}>Přihlásit</button>}
                {this.state.access_token != null && <button onClick={() => { this.getInitial() }}>Přihlásit</button>}

                <ul>
                    {Object.keys(this.state).map((key) =>
                        <li key={key}>
                            {
                                typeof this.state[key] !== 'object' && key + ': ' + this.state[key]
                            }
                        </li>
                    )}
                </ul>
            </div>
        )
    }
}

export default Location

