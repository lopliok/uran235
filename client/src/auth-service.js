import axios from 'axios'

export default {

  user: null,

  onChange: null,

  async init() {
    this.reload()
  },

  async reload() {

    const res = await axios.get('/logged')
    this.user = res.data.isLoggedIn ? 'Uran235' : null

    console.log(this.user)

    this.onChange()

  },

  isLoggedIn() {
    return this.user !== null
  },

  async login(credentials) {

    await axios.post('/login', credentials)
    this.reload()
 
  },

  logout() {
    axios.get('/logout').then((r) => {
      this.reload()
      
    })
  }

}
