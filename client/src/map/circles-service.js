import axios from 'axios'


export default {
    

    loadCircles() {
       return axios.get('/circles')
    },

    deleteTeam(id) {
        return axios.delete('/teams/' + id)
     },

     trackTeam(body) {
        
        return axios.put('/teams/' + body._id , body)
     },

     newTeam(body) {
        return axios.post('/teams', body )
     }

     


   
}