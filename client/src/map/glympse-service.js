import axios from 'axios'


export default {


    getCredentials(api_key) {
        return axios.post('https://api.glympse.com/v2/account/create?api_key=' + api_key );
    },

    getAccessToken(userId, password, api_key) {
        return axios.post('https://api.glympse.com/v2/account/login?username=' + userId +'&password=' + password + '&api_key=' + api_key)
    },
   
    initialLocation(codeTeam, config) {
        
        return axios.get('https://api.glympse.com/v2/invites/' + codeTeam + '/?next=0&uncompressed=true', config)
    },

    pollingLocation(codeTeam, next, config) {
        
        return axios.get('https://api.glympse.com/v2/invites/' + codeTeam + '/?next=' + next + '&uncompressed=true', config)
     }

   
}