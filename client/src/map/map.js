import React, { Component } from 'react'
import { Circle, CircleMarker, Polygon, Polyline, Popup, Rectangle, TileLayer, Marker, Map as LeafletMap, Icon, Tooltip } from 'react-leaflet'
import moment from 'moment'
import update from 'react-addons-update';
import _ from 'lodash'
import MyPolyline from './../components/polyline'

import circleService from './circles-service'

import teamService from './../teams/teams-service'
import glympseComp from './glympse-comp'

import utils from '../utils/map-utils'

function geolocationErrorHandler(err) {
    const { code } = err
    switch (code) {
        case 1:
            console.log('Permission denied -- please allow geolocation')
            break
        case 2:
            console.log('Position unavailable -- try again later')
            break
        case 3:
            console.log('Position querying timed out -- try again later')
            break
        default:
            console.log('Unknown error happened while querying position')
            break
    }
}

class Map extends Component {

    constructor(props) {
        super(props)

        this.state = {
            viewport: {
                center: [
                    49.01935260040647,
                    15.189285278320314
                ],
                zoom: 13
            },
            teams: [],
            markerPosition: [51.505, -0.09],
            circle: [51.505, -0.09],
            circles: [

            ]
        }
        this.handleClickOnMap = this.handleClickOnMap.bind(this)
    }

    async loadTeams() {
        const res = await teamService.loadTeams();
        const res2 = await circleService.loadCircles();
        res.data.map((team) => team.firstTrack = true)
        this.setState({
            teams: res.data,
            circles: res2.data
        })

    }

    find = (array, id) => {
        return _.findIndex(array, function (i) { return i._id == id; });
    }


    previousTeamsLocation() {
        this.state.teams.forEach(async (team, idx) => {

            if (team.tracked) {

                const res = await glympseComp.initialLocation(team.code)


                let array = utils.getSubArraysLocation(res.location, 0, 3)
                let last = res.location.slice(-1)[0]
                let [lat, long] = utils.converterCoords(last[1], last[2])

                let time = moment(new Date(last[0])).fromNow();

                //console.log(time)



                if (team.firstTrack) {
                    let polyline = utils.getSubArraysLocation(array, 1, 3)
                    polyline = polyline.map((item) => { return utils.converterCoords(item[0], item[1]) })

                    this.setState((prevState) => {

                        const index = this.find(prevState.teams, team._id)

                        const newTeams = [...prevState.teams]

                        newTeams[index].timeString = time
                        newTeams[index].route = polyline
                        newTeams[index].marker = [lat, long]
                        newTeams[index].firstTrack = false

                        return {
                            viewport: { center: [lat, long] },
                            markerPosition: [lat, long],
                            teams: newTeams
                        }
                    })
                } else {

                    this.setState(prevState => {
                        const index = this.find(this.state.teams, team._id)

                        let newTeams = [...prevState.teams]

                        newTeams[index].route = [...newTeams[index].route, [lat, long]]
                        newTeams[index].marker = [lat, long]

                        return {
                            teams: newTeams,
                        }
                    })
                }



            }




        })

    }

    componentWillMount() {
        this.loadTeams().then(() => {
            this.previousTeamsLocation()
        })

        this.interval = setInterval(() => {
            this.forceUpdate();
            this.previousTeamsLocation()
        }, 20000);

        if (navigator && navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                res => {
                    this.updateCoords(res.coords.latitude, res.coords.longitude)
                },
                err => {
                    geolocationErrorHandler(err)
                }
            )
        }
    }
    updateCoords(lat, lng) {
        this.setState({
            markerPosition: [
                lat,
                lng
            ]
        })
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    handleClickOnMap(e) {

        console.log('{\n "name": "",\n "point": \n[' + e.latlng.lat + ', ' + e.latlng.lng + '],\n "radius": 200,\n "active": true \n}')

    }







    render() {

        //let polyline = this.state.teams

        return (
            <div>
                <section className="content">

                    <div className="card">
                        <div className="card-header">
                            <h3 className="card-title">Map</h3>
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i className="fa fa-minus"></i></button>
                                <button type="button" className="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i className="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div className="card-body">
                            <LeafletMap
                                length={4}
                                viewport={this.state.viewport}
                                onClick={this.handleClickOnMap}
                            >
                                <TileLayer
                                    attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                />
                                {this.state.circles.map((circle, idx) =>
                                    <React.Fragment key={idx}>

                                        {circle.point && <Circle center={circle.point} key={idx} radius={circle.radius} >
                                            <Popup>
                                                <div>
                                                    <div>Team: {circle.name}</div>
                                                    <p > radius:{circle.radius} </p>
                                                    <p > active: true{circle.active} </p>
                                                    <p > id:{circle._id} </p>

                                                </div>
                                            </Popup>
                                        </Circle>}
                                    </React.Fragment>
                                )}
                                <Marker position={this.state.markerPosition}>
                                    <Popup>
                                        <div>
                                            To jsem já.
                                                </div>
                                    </Popup>
                                </Marker>
                                {this.state.teams.map((team, idx) =>
                                    <React.Fragment key={idx}>
                                        {team.marker && <Marker position={team.marker}>
                                            <Popup>
                                                <div>
                                                    <div>Team: {team.name}</div>
                                                    {team.players.map((player, idx) =>
                                                        <span key={idx}>{player} </span>
                                                    )}
                                                    <div>Last update: {team.timeString}</div>
                                                </div>
                                            </Popup>
                                        </Marker>}
                                        <MyPolyline team={team} key={idx} color={team.color} />
                                    </React.Fragment>
                                )}
                            </LeafletMap>
                        </div>
                        <div className="card-footer">

                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default Map

