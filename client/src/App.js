import { Link, Route, HashRouter as Router, Switch } from "react-router-dom";
import React, { Component } from "react";

import Aside from "./aside";
import Axios from "axios";
import Header from "./header";
import Location from "./Location/location";
import Login from "./login";
import Map from "./map/map";
import MapStored from "./map-stored/map";
import ROUTES from "./routes";
import Tasks from "./tasks/tasks";
import Teams from "./teams/teams";
import TeamsDetail from "./teams/teams-detail";
import TeamsNew from "./teams/teams-new";
import TestMap from "./map/testmap";
import authService from "./auth-service";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <React.Fragment>
            <Route
              path="/"
              render={() => (
                <React.Fragment>
                  {!authService.isLoggedIn() && <Login />}

                  {authService.isLoggedIn() && (
                    <div>
                      <Header />
                      <Aside />
                      <div
                        className="content-wrapper"
                        style={{ minHeight: "664px" }}
                      >
                        <Switch>
                          <Route exact path="/" component={Tasks} />
                          <Route path={ROUTES.MAP} component={Map} />
                          <Route path={ROUTES.MAP_LIVE} component={MapStored} />
                          <Route path="/test-map" component={TestMap} />
                          <Route path={ROUTES.TEAMS_NEW} component={TeamsNew} />
                          <Route
                            path={ROUTES.TEAMS_DETAIL}
                            component={TeamsDetail}
                          />
                          <Route
                            path={ROUTES.TEAMS_LISTING}
                            component={Teams}
                          />
                        </Switch>
                      </div>
                    </div>
                  )}
                </React.Fragment>
              )}
            />
          </React.Fragment>
        </Router>
      </div>
    );
  }
}

export default App;

const style = {
  listStyleType: "none"
};
