import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import teamService from './teams-service'
import ROUTES from './../routes'

class TeamNew extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            team: {},

            newtrack: ''
        }

        this.onChangeTextArea = this.onChangeTextArea.bind(this)
        this.onSave = this.onSave.bind(this)
        this.onChange = this.onChange.bind(this)
        this.setNewTrack = this.setNewTrack.bind(this)


    }



    componentWillMount() {
        this.load(this.props.match.params.id)

    }

    componentWillReceiveProps(newProps) {
        if (this.props.match.params.id !== newProps.match.params.id) {
            this.load(newProps.match.params.id)
        }
    }

    async load(id) {

        const res = await teamService.loadById(id)

        this.setState({
            team: res.data
        })
        console.log(this.state.team)
    }

    onChangeTextArea(e) {
        const note = e.target.value
        e.preventDefault()

        this.setState((prevState, e) => {
            let team = {...prevState.team}
            team.note = note

            return {team: team};
          });
    }

    async onSave() {
        const res = await teamService.trackTeam(this.state.team)  
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    async setNewTrack() {

        let team = {...this.state.team}
        team.lastcode = team.code
        team.code = this.state.newtrack

        this.setState({
            team: team
        })
        
        await teamService.trackTeam(team)
        console.log(this.props)
    }



    render() {


        return (
            <div>
                <div className="card" style={{width: '18rem'}}>
                        <div className="card-body">
                       <h3> Team editor </h3>
                            <h5 className="card-title">Team <b>{this.state.team.name}</b></h5>
                            <p className="card-text">tracking code: {this.state.team.code}</p>
                            <p className="card-text">Members: {this.state.team.players}</p>
                            <p className="card-text">note: <textarea onChange={this.onChangeTextArea} value={this.state.team.note}></textarea></p>
Musí být uloženo.

                            <button className="btn btn-primary" onClick={this.onSave}>Save</button>

<br />
<br />
<p className="card-text">new code: <input name='newtrack' onChange={this.onChange} value ={this.newtrack} /><button className="btn btn-primary btn-sm" onClick={this.setNewTrack}>Change track code</button>
</p>
                            <Link to={ROUTES.TEAMS_LISTING} className="btn btn-secondary">Teams listing</Link>
                            <Link to={ROUTES.MAP} className="btn btn-secondary">Map</Link>
                        </div>
                    </div>
                </div>

                );
            }
        }
        
        
        
        
        export default TeamNew
        
