import axios from 'axios'


export default {
    

    loadById(id) {
        return axios.get('/teams/' + id)
     },

    loadTeams() {
       return axios.get('/teams')
    },

    deleteTeam(id) {
        return axios.delete('/teams/' + id)
     },

     trackTeam(body) {
        
        return axios.put('/teams/' + body._id , body)
     },

     newTeam(body) {
        return axios.post('/teams', body )
     }

     


   
}