import React, { Component } from 'react'
import { Link } from 'react-router-dom'



import teamsService from './teams-service'

import TeamsNew from './teams-new'

import ROUTES from '../routes'

class Teams extends Component {

    constructor(props) {
        super(props)

        this.state = {
            teams: [],
            done: 55
        }
    }

    componentWillMount() {
        this.load()
    }


    async load() {
        const res = await teamsService.loadTeams();
        this.setState({
            teams: res.data
        })
    }
    async delete(id) {
        const res = await teamsService.deleteTeam(id);
        this.load()

    }

    async delete(id) {
        const res = await teamsService.deleteTeam(id);
        this.load()

    }

    async setTracker(team) {
        team.tracked = !team.tracked

        const res = await teamsService.trackTeam(team);
        this.load()
    }

    handleDelete(teamId) {
        this.delete(teamId)
    }

    handleClick(team) {
        this.setTracker(team);
    }


    getUrl(path, params = {}) {
        return path.replace(/:(\w+)/g, (m, k) => params[k])
    }


    render() {


        return (

            <div>

                <section className="content">

                    <div className="card">
                        <div className="card-header">
                            <h3 className="card-title">Map</h3>

                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i className="fa fa-minus"></i></button>
                                <button type="button" className="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i className="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div className="card-body">

                            <div className="card">
                                <div className="card-header">
                                    <h3 className="card-title">Teams listing</h3>
                                </div>
                                <div className="card-body p-0">
                                    <table className="table table-condensed table-responsive">
                                        <tbody>
                                            <tr>
                                                <th style={{ width: "120px" }}>Kód týmu</th>
                                                <th>Jméno týmu</th>
                                                <th>Team color</th>
                                                <th>Progress</th>
                                                <th style={{ width: "260px" }}>Label</th>
                                            </tr>
                                            {this.state.teams.map((team) =>
                                                <tr key={team._id}>
                                                    <td>{team.code}</td>
                                                    <td>
                                                        <Link to={this.getUrl(ROUTES.TEAMS_DETAIL, {id: team._id})}>{team.name}
                                                        </Link>
                                                    </td>
                                                    <td>{team.color}</td>
                                                    <td> {team.progress ?
                                                        <div className="progress" style={{ height: '10px' }}>
                                                            <div className="progress-bar progress-bar-striped bg-success" style={{ width: team.progress + '%' }}></div>
                                                        </div>
                                                        : ''}</td>
                                                    <td>
                                                        <button className='btn btn-info btn-sm' onClick={() => this.handleClick(team)} >{!team.tracked ? 'Sledovat' : 'Přestat sledovat'}</button>

                                                        <button className='btn btn-sm btn-danger' onClick={() => { this.handleDelete(team._id) }}>Smazat</button>
                                                    </td>
                                                </tr>
                                            )}
                                        </tbody></table>
                                </div>
                            </div>
                        </div>
                        <div className="card-footer">
                            Teams listing
                        </div>
                    </div>

                </section>

            </div>


        )
    }
}



export default Teams

