import axios from 'axios'


export default {
    

    loadById(id) {
        return axios.get('/tasks/' + id)
     },

    loadTasks() {
       return axios.get('/tasks')
    },

    deleteTask(id) {
        return axios.delete('/tasks/' + id)
     },

     likeTask(body) {
        
        return axios.put('/tasks/' + body._id , body)
     },

     newTask(body) {
        return axios.post('/tasks', body )
     }

     


   
}