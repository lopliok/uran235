import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { TwitterPicker } from 'react-color';

import tasksService from './tasks-service'

import InputElement from '../components/input'




class TaskNew extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            code: '',
            color: '',
            members: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handleColor = this.handleColor.bind(this)

    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleColor(e) {
        this.setState({ color: e.hex })
    }

    handleClick(e) {
        e.preventDefault()
        this.send().then(res => {
            this.props.history.push('teams/')
        })
    }

    async send() {
        const team = {
            name: this.state.name,
            code: this.state.code,
            color: this.state.color,
            tracked: false,
            firstTracking: true,
            progress: Math.floor(Math.random() * 101),
            players: this.state.members.split(';').map(x => x.replace(' ', ''))
        }
        await tasksService.newTeam(team)
    }









    render() {


        return (
            <div>
                <section className="content">

                    <div className="card">
                        <div className="card-header">
                            <h3 className="card-title">New team</h3>

                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i className="fa fa-minus"></i></button>
                                <button type="button" className="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i className="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div className="card-body row">

                            <div className="col-md-6">
                                <div className="card card-primary">
                                    <div className="card-header" style={{ height: '47px' }}>
                                        <h3 className="card-title">Add new team</h3>
                                    </div>
                                    <form role="form">
                                        <div className="card-body">

                                            <InputElement name='name' label='Název týmu' handleChange={this.handleChange} />
                                            <InputElement name='code' label='Kód týmu' handleChange={this.handleChange} />
                                            <div className="form-group form-check">
                                                <label className="form-check-label">
                                                    Barva:
                                                <TwitterPicker onChange={this.handleColor} name='color' />
                                                </label>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="comment">Členové týmu oddělené středníkem:</label>
                                                <textarea className="form-control" onChange={this.handleChange} rows="5" id="comment" name='members'></textarea>
                                            </div>
                                        </div>

                                        <div className="card-footer">
                                            <button type="submit" className="btn btn-primary">Submit</button>
                                        </div>
                                    </form>
                                </div>


                            </div>
                            <div className="col-md-6">
                                <div className="card">
                                    <div className="card-header">
                                        <h5 className="m-0">Team:  {this.state.name} </h5>
                                    </div>
                                    <div className="card-body">
                                        <h6 className="card-title"></h6>

                                        <span className="card-text">
                                            <div> Name: {this.state.name} </div>
                                            <div> Code: {this.state.code} </div>
                                            <div> Members: {this.state.members.replace(';', ' ')} </div>
                                            <div> Color: {this.state.color} </div>


                                        </span>
                                        <br />
                                        <a href="#" className="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div className="card-footer">
                            Footer
                </div>
                    </div>

                </section>

            </div>

        );
    }
}




export default TaskNew

