import React, { Component } from 'react'
import { Link } from 'react-router-dom'



import tasksService from './tasks-service'

import TasksNew from './tasks-new'

import ROUTES from '../routes'

class Tasks extends Component {

    constructor(props) {
        super(props)

        this.state = {
            tasks: []
        }
    }

    componentWillMount() {
        this.load()
    }


    async load() {
        const res = await tasksService.loadTasks();
        this.setState({
            tasks: res.data
        })
    }


    async delete(id) {
        const res = await tasksService.deleteTask(id);
        this.load()

    }

    async like(task) {
      
       
        const res = await tasksService.likeTask(task);
        this.load()
    }

    handleDelete(taskId) {
        this.delete(taskId)
    }

    handleClick(task) {
        this.setTracker(task);
    }


    getUrl(path, params = {}) {
        return path.replace(/:(\w+)/g, (m, k) => params[k])
    }


    render() {


        return (

            <div>

                <section className="content">

                    <div className="card">
                        <div className="card-header">
                            <h3 className="card-title">Map</h3>

                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i className="fa fa-minus"></i></button>
                                <button type="button" className="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i className="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div className="card-body">

                            <div className="card">
                                <div className="card-header">
                                    <h3 className="card-title">Teams listing</h3>
                                </div>
                                <div className="card-body p-0">
                                    <table className="table table-condensed">
                                        <tbody>
                                            <tr>
                                                <th style={{ width: "120px" }}>Název ůkolu</th>
                                                <th>Jméno týmu</th>
                                                <th>Progress</th>
                                                <th style={{ width: "260px" }}>Label</th>
                                            </tr>
                                            {this.state.tasks.map((task) =>
                                                <tr key={task._id}>
                                                    <td>{task.name}</td>
                                                    <td>{task.description}</td>
                                                    <td> {task.likes} </td>
                                                        
                                                    <td>
                                                        <button className='btn btn-info btn-sm' onClick={() => this.handleClick(task)} >{!task.liked ? 'Like' : 'Unlike'}</button>

                                                        <button className='btn btn-sm btn-danger' onClick={() => { this.handleDelete(task._id) }}>Smazat</button>
                                                    </td>
                                                </tr>
                                            )}
                                        </tbody></table>
                                </div>
                            </div>
                        </div>
                        <div className="card-footer">
                            Footer
</div>
                    </div>

                </section>

            </div>


        )
    }
}



export default Tasks

