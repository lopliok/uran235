import React, {Component} from 'react'
import authService from './auth-service'

class Login extends Component {

    constructor(props) {
        super(props)

        this.state = {
            login: '',
            password: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.login = this.login.bind(this)
    }
    
    login(e) {
        authService.login(this.state);
        e.preventDefault();
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }











    render() {
        return (
            <div className="login-box">
                <div className="login-logo">
                    <a href="../../index2.html"><b>Admin</b>LTE</a>
                </div>
                <div className="card">
                    <div className="card-body login-card-body">
                        <p className="login-box-msg">Sign in to start your session</p>

                        <form action="../../index2.html" method="post" onSubmit={this.login}>
                            <div className="form-group has-feedback">
                                <input type="email" className="form-control" name="login" placeholder="Email" value={this.state.login} onChange={this.handleChange} />
                                <span className="fa fa-envelope form-control-feedback"></span>
                            </div>
                            <div className="form-group has-feedback">
                                <input type="password" className="form-control" name="password" placeholder="Password" value={this.state.password} onChange={this.handleChange} />
                                <span className="fa fa-lock form-control-feedback"></span>
                            </div>
                            <div className="row">
                                <div className="col-8">
                                    <div className="checkbox icheck">
                                        <label>
                                            <div className="icheckbox_square-blue" aria-checked="false" aria-disabled="false" style={{ position: 'relative' }}>
                                                <input type="checkbox"  />
                                                <ins className="iCheck-helper">
                                                </ins>
                                            </div> Remember Me
              </label>
                                    </div>
                                </div>
                                <div className="col-4">
                                    <button type="submit" className="btn btn-primary btn-block btn-flat" onClick={this.login} >Sign In</button>
                                </div>
                            </div>
                        </form>
                        <p className="mb-1">
                            <a href="#">I forgot my password</a>
                        </p>
                        <p className="mb-0">
                            <a href="register.html" className="text-center">Register a new membership</a>
                        </p>
                    </div>
                </div>
            </div>

        )



    }
}


export default Login