import {
  Circle,
  CircleMarker,
  Icon,
  Map as LeafletMap,
  Marker,
  Polygon,
  Polyline,
  Popup,
  Rectangle,
  TileLayer
} from "react-leaflet";
import React, { Component } from "react";

const MyPolyline = ({ team, color }) => {
  if (team.route) {
    return (
      <Polyline color={color} positions={team.route} key={`${team._id}`} />
    );
  } else {
    return null;
  }
};

export default MyPolyline;
