import React, { Component } from 'react';



class InputElement extends Component {

    render() {
        return (
            <div className="form-group">
                <label htmlFor="name">{this.props.label}</label>
                <input type="text" className="form-control" onChange={this.props.handleChange} name={this.props.name} id='name' />
            </div>
        )
    }

}

export default InputElement;