import React, { Component } from 'react'
import { Circle, CircleMarker, Polygon, Polyline, Popup, Rectangle, TileLayer, Marker, Map as LeafletMap, Icon } from 'react-leaflet'

import update from 'react-addons-update';
import _ from 'lodash'
import MyPolyline from './../components/polyline'

import teamService from './../teams/teams-service'
import glympseComp from './glympse-comp'

import utils from '../utils/map-utils'


class TeamIndicator extends Component {

    constructor(props) {
        super(props)

        this.state = {

            teams: [],
        }
    }

    async loadTeams() {
        const res = await teamService.loadTeams();
        res.data.map((team) => team.firstTrack = true)
        this.setState({
            teams: res.data
        })

    }

    find = (array, id) => {
        return _.findIndex(array, function (i) { return i._id == id; });
    }

    previousTeamsLocation() {
        this.state.teams.forEach(async (team, idx) => {

            if (team.tracked) {

                const res = await glympseComp.initialLocation(team.code)

                let array = utils.getSubArraysLocation(res.location, 0, 3)
                let last = res.location.slice(-1)[0]
                let [lat, long] = utils.converterCoords(last[1], last[2])

                // it can track only one team yet.
                // TODO: tracking for multiple teams


                if (team.firstTrack) {
                    let polyline = utils.getSubArraysLocation(array, 1, 3)
                    polyline = polyline.map((item) => { return utils.converterCoords(item[0], item[1]) })

                    this.setState((prevState) => {

                        const index = this.find(prevState.teams, team._id)

                        const newTeams = [...prevState.teams]

                        newTeams[index].route = polyline
                        newTeams[index].marker = [lat, long]
                        newTeams[index].firstTrack = false

                        return {
                            viewport: { center: [lat, long] },
                            markerPosition: [lat, long],
                            teams: newTeams
                        }
                    })
                } else {

                    this.setState(prevState => {
                        const index = this.find(this.state.teams, team._id)

                        let newTeams = [...prevState.teams]

                        newTeams[index].route = [...newTeams[index].route, [lat, long]]
                        newTeams[index].marker = [lat, long]


                        return {
                            teams: newTeams,
                        }
                    })
                }
            }
        })
    }

    componentWillMount() {
        this.loadTeams().then(() => {
            this.previousTeamsLocation()
        })

        this.interval = setInterval(() => {
            this.forceUpdate();
            this.previousTeamsLocation()
        }, 5000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    
    render() {

        //let polyline = this.state.teams

        return (
            <div>
               {this.state.teams.map((team, idx) =>
                                    <React.Fragment>
                                        
                                        <p key={idx} > fdsf </p>
                                    </React.Fragment>
                                )}
            </div>
        )
    }
}

export default TeamIndicator

