const axios = require("axios");

let Glympse = {
  api_key: "0156a3in2FrS1kxwurOD",
  password: null,
  user_ID: null,
  time_to: null,
  access_token: null
};

const getAccessToken = async () => {
  let res = await axios.post(
    "http://api.glympse.com/v2/account/create?api_key=" + Glympse.api_key
  );

  Glympse.password = res.data.response.password;
  Glympse.user_ID = res.data.response.id;

  res = await axios.post(
    "http://api.glympse.com/v2/account/login?username=" +
      Glympse.user_ID +
      "&password=" +
      Glympse.password +
      "&api_key=" +
      Glympse.api_key
  );

  Glympse.time_to = res.data.response.expires_in;
  Glympse.access_token = res.data.response.access_token;
  Glympse.time_from = res.data.meta.time;
};

const initialTicket = async codeTeam => {
  let config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + Glympse.access_token
    }
  };

  const res = await axios.get(
    "http://api.glympse.com/v2/invites/" +
      codeTeam +
      "/?next=0&uncompressed=true",
    config
  );
  console.log(res.data);
  return res.data;
};

const pollingLocation = (codeTeam, next) => {
  let config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + Glympse.access_token
    }
  };

  return axios.get(
    "https://api.glympse.com/v2/invites/" +
      codeTeam +
      "/?next=" +
      next +
      "&uncompressed=true",
    config
  );
};

module.exports = {
  getAccessToken,
  initialTicket,
  pollingLocation
};
