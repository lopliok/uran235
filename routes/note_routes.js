var ObjectID = require("mongodb").ObjectID;

module.exports = function(app, db) {
  app.get("/notes/:id", (req, res) => {
    const id = req.params.id;
    const details = { _id: new ObjectID(id) };
    db.collection("notes").findOne(details, (err, item) => {
      if (err) {
        res.send({ error: "An error has occurred" });
      } else {
        res.send(item);
      }
    });
  });

  app.get("/notes", (req, res) => {
    db.collection("notes")
      .find({})
      .toArray(function(err, result) {
        if (err) throw err;
        res.send(result);
      });
  });

  app.delete("/notes/:id", (req, res) => {
    const id = req.params.id;
    const details = { _id: new ObjectID(id) };
    db.collection("notes").remove(details, (err, item) => {
      if (err) {
        res.send({ error: "An error has occurred" });
      } else {
        res.send("Note " + id + " deleted!");
      }
    });
  });

  app.put("/notes/:id", (req, res) => {
    const id = req.params.id;
    const details = { _id: new ObjectID(id) };
    const note = { text: req.body.body, title: req.body.title };
    db.collection("notes").update(details, note, (err, result) => {
      if (err) {
        res.send({ error: "An error has occurred" });
      } else {
        res.send(note);
      }
    });
  });

  app.post("/notes", (req, res) => {
    const note = { text: req.body.body, title: req.body.title };
    db.collection("notes").insert(note, (err, result) => {
      if (err) {
        res.send({ error: "An error has occurred" });
      } else {
        res.send(result.ops[0]);
      }
    });
  });

  app.get("/teams/:id", (req, res) => {
    const id = req.params.id;
    const details = { _id: new ObjectID(id) };
    db.collection("teams").findOne(details, (err, item) => {
      if (err) {
        res.send({ error: "An error has occurred" });
      } else {
        res.send(item);
      }
    });
  });

  const sortByTimestamp = obj => {
    if (!obj) return [];
    return obj.sort((it, prev) => it > prev);
  };

  app.get("/location/:id", (req, res) => {
    const id = req.params.id;
    const details = { _id: new ObjectID(id) };

    console.log(id);

    db.collection("locations")
      .find({ team_id: new ObjectID(id) })
      .toArray(function(err, result) {
        if (err) throw err;
        res.send(result);
      });

    /*  .toArray((err, result) => {
        if (err) {
          res.send({ error: "An error has occurred" });
        } else {
          console.log(result);
          res.send({ result });
        }
      }); */
  });

  app.get("/teams", (req, res) => {
    db.collection("teams")
      .find({})
      .toArray(function(err, result) {
        if (err) throw err;
        res.send(result);
      });
  });

  app.post("/teams", (req, res) => {
    db.collection("teams").insertOne(req.body, (err, result) => {
      if (err) {
        res.send({ error: "An error has occurred" });
      } else {
        res.send(result.ops[0]);
      }
    });
  });

  app.put("/teams/:id", (req, res) => {
    const id = req.params.id;
    const details = { _id: new ObjectID(id) };
    let team = {
      ...req.body
    };
    delete team._id;

    db.collection("teams").update(details, team, (err, result) => {
      if (err) {
        res.send({ error: "An error has occurred" });
      } else {
        res.send(team);
      }
    });
  });

  app.delete("/teams/:id", (req, res) => {
    const id = req.params.id;
    const details = { _id: new ObjectID(id) };
    db.collection("teams").remove(details, (err, item) => {
      if (err) {
        res.send({ error: "An error has occurred" });
      } else {
        res.send("Note " + id + " deleted!");
      }
    });
  });
};
