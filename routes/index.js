const noteRoutes = require('./note_routes');
const taskRoutes = require('./task_routes')
const circleRoutes = require('./circle_routes')

module.exports = function(app, db) {
  noteRoutes(app, db);
  taskRoutes(app, db);
  circleRoutes(app, db);


  // Other route groups could go here, in the future
};