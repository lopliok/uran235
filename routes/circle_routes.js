var ObjectID = require('mongodb').ObjectID;


module.exports = function (app, db) {
  



  app.get('/circles/:id', (req, res) => {
    const id = req.params.id
    const details = { '_id': new ObjectID(id) };
    db.collection('circles').findOne(details, (err, item) => {
      if (err) {
        res.send({ 'error': 'An error has occurred' });
      } else {
        res.send(item);
      }
    });
  });



  app.get('/circles', (req, res) => {
    db.collection('circles').find({}).toArray(function (err, result) {
      if (err) throw err;
      res.send(result)
    });

  });



  app.post('/circles', (req, res) => {
    db.collection('circles').insertOne(req.body, (err, result) => {
      if (err) {
        res.send({ 'error': 'An error has occurred' });
      } else {
        res.send(result.ops[0]);
      }
    });
  });

  app.put('/circles/:id', (req, res) => {
    const id = req.params.id;
    const details = { '_id': new ObjectID(id) };
    let team = {
      ...req.body
    }
    delete team._id;

    db.collection('circles').update(details, team, (err, result) => {
      if (err) {
        res.send({ 'error': 'An error has occurred' });
      } else {
        res.send(team);
      }
    });
  });

  app.delete('/circles/:id', (req, res) => {
    const id = req.params.id;
    const details = { '_id': new ObjectID(id) };
    db.collection('circles').remove(details, (err, item) => {
      if (err) {
        res.send({ 'error': 'An error has occurred' });
      } else {
        res.send('Note ' + id + ' deleted!');
      }
    });
  });



};