const axios = require("axios");

const express = require("express");
const path = require("path");
const generatePassword = require("password-generator");
const http = require("http");

const MongoClient = require("mongodb").MongoClient;
const bodyParser = require("body-parser");
const db = require("./config/db");

var WebSocketServer = require("ws").Server;
var cors = require("cors");

var glympse = require("./glympse");

var userLogged = true;
const app = express();

app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var server = http.createServer(app);

// Serve static files from the React app
app.use(express.static(path.join(__dirname, "client/build")));

// First authetication
app.get("/logged", (req, res) => {
  res.send({ isLoggedIn: userLogged });
});

app.post("/login", (req, res) => {
  if (req.body.login == "Uran235" && req.body.password == "Uran235") {
    userLogged = true;
    res.send({ isLoggedIn: userLogged });
  } else {
    res.send({ Error: "Bad password or login" });
  }
});

app.get("/logout", (req, res) => {
  userLogged = false;
  res.send({ isLoggedIn: userLogged });
});

const port = process.env.PORT || 8000;

MongoClient.connect(db.url, (err, database) => {
  if (err) return console.log(err);
  require("./routes")(app, database);
  server.listen(port, () => {
    console.log("We are live on " + port);
  });
});

/*

Websocket test

wss = new WebSocketServer({ server: server, path: '/live' })



wss.on('connection', function (ws) {
    ws.on('message', function (message) {
        console.log('received: %s', message)
    })

    setInterval(
        () => ws.send(`${new Date()}`),
        5000
    )


})


*/

const saveLocation = (db, locpoints, id) => {
  const list = locpoints.map(it => ({
    team_id: id,
    timestamp: it[0],
    latitude: it[1],
    longitude: it[2],
    speed: it[3],
    heading: it[4],
    elevation: it[5],
    horizontal_accuracy: it[6],
    vertical_acuuracy: it[7]
  }));

  try {
    db.collection("locations").insert(list, (err, result) => {
      // console.log(result);
    });
  } catch (e) {
    console.log(e);
  }
};

const updateTeam = (db, teamUpdate, name) => {
  try {
    db.collection("teams").updateOne(
      { name: name },
      {
        $set: teamUpdate
      }
    );
  } catch (e) {
    console.log(e);
  }
};

async function run() {
  const db = await MongoClient.connect(
    "mongodb://lopliok:2578319@ds229909.mlab.com:29909/lopliok-app"
  );

  await glympse.getAccessToken();

  db.collection("teams")
    .find({})
    .toArray(function(err, result) {
      if (err) throw err;

      for (let index = 0; index < result.length; index++) {
        const team = result[index];

        if (team.tracked) {
          if (team.firstTracking) {
            glympse.initialTicket(team.code).then(res => {
              const start_time = res.response.properties[0].v;
              const locations = res.response.location;
              const { first, next, last } = res.response;

              const teamUpdate = {
                start_time,
                firstTracking: false,
                first,
                next,
                last
              };

              updateTeam(db, teamUpdate, team.name);

              saveLocation(db, locations, team._id);
            });
          } else {
            glympse.pollingLocation(team.code, team.next).then(res => {
              const data = res.data.response;

              const { location, first, next, last } = data;

              const teamUpdate = {
                first,
                next,
                last
              };

              if (location) {
                updateTeam(db, teamUpdate, team.name);

                saveLocation(db, location, team._id);
              } else {
                console.log("new data no available", location);
              }
            });

            // console.log(res.response);
            /*  try {
              db.collection("teams").updateOne(
                { name: team.name },
                {
                  $set: {
                    trackek: start_time
                  }
                }
              );
            } catch (e) {
              console.log(e);
            } */
          }
        }
      }
    });
}

//run();

setInterval(run, 30000);
